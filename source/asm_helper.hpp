#pragma once

#define SAVE_RETURN_ADDRESS_TO(x) void ** puEBP = NULL;       \
                                  __asm { mov puEBP, ebp };   \
                                  void * x = puEBP[1];