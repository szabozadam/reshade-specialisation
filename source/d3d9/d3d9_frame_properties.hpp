#pragma once

namespace reshade::d3d9
{
  struct d3d9_frame_properties
  {
    bool pre_ui_rendered = false;
    bool post_ui_rendered = false;
    bool force_all_render = false;
  };
}