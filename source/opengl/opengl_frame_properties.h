#pragma once

namespace reshade::opengl
{
  struct opengl_frame_properties
  {
    bool pre_ui_rendered = false;
    bool post_ui_rendered = false;
    bool force_all_render = false;
  };
}