#pragma once

enum upper_operation_hint
{
  ShouldDiscard,
  ShouldCallProperly
};