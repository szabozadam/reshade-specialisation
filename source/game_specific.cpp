#pragma once
#include "ini_file.hpp"
#include "game_specific.hpp"
#include <vector>

reshade::game_specific_configuration::game_specific_configuration()
{

}
void reshade::game_specific_configuration::on_dll_attach()
{
  load_settings();
}
void reshade::game_specific_configuration::on_dll_detach()
{
  save_settings();
}
void reshade::game_specific_configuration::save_settings()
{
  ini_file config(settings.game_settings_ini_file_name);

  for (const auto& setting : settings)
  {
    config.set("GAME_SETTINGS", setting->name, { setting->value });
  }
}
void reshade::game_specific_configuration::load_settings()
{
  const ini_file config(settings.game_settings_ini_file_name);

  for (auto& setting : settings)
  {
    setting->value = config.get("GAME_SETTINGS", setting->name, { setting->default_value }).as<float>();
  }
}
void reshade::game_specific_configuration::on_graphics_inited()
{

}
float reshade::game_specific_configuration::value_of_name(const char* name)
{
  for (auto& setting : settings)
  {
    if (_stricmp(name, setting->name) == 0)
      return setting->value;
  }
  return 0;
}
reshade::game_specific_configuration reshade::current_game_configuration;