#pragma once
#include <vector>
#include <memory>

namespace reshade
{
  //everything will be represented as floats
  struct game_specific_setting
  {
    const char* name;
    float default_value;
    float min_value;
    float max_value;
    float step;
    float value;
    float previous_value;
  };

  class game_specific_setting_collection
  {
  public:
    //place for individual game_specific_setting-s

  private:
    std::vector<std::shared_ptr<game_specific_setting>> settings
    {
    };

  public:
    const char* game_settings_category_name = "Game specific settings";
    const char* game_settings_ini_file_name = "GameSpecificSettings.ini";

    auto begin()
    {
      return settings.begin();
    }
    auto end()
    {
      return settings.end();
    }
    auto size()
    {
      return settings.size();
    }
  };

  struct game_specific_configuration
  {
    game_specific_setting_collection settings;

    void on_graphics_inited();
    void on_dll_attach();
    void on_dll_detach();
    game_specific_configuration();
    void save_settings();
    void load_settings();
    float value_of_name(const char* name);
  };
  extern game_specific_configuration current_game_configuration;
}